import React from 'react'

export default function About() {
  return (
    <section className="section-about">
        <div className="u-center-text">
                <h2 className="heading-secondary u-margin-bottom-big">
                    Exiting tours for adventurous people
                </h2>
        </div>

        <div className="row">
            <div className="col-1-of-2">
                <h3 className="heading-tertiary u-margin-bottom-small">
                    You are all going to fall in love with nature.
                </h3>
                <p className="paragraph">
                    Lorem ipsum . Ex ducimus laboriosam inventore praesentium tempora tenetur, nisi veritatis consequuntur commodi voluptatibus quo itaque nesciunt placeat eligendi nobis! Delectus voluptates fuga praesentium.
                </p>
                <h3 className="heading-tertiary  u-margin-bottom-small">
                    You are all going to fall in love with nature.
                </h3>
                <p className="paragraph">
                    Ametaepe labore officiis, nostrum eveniet, deleniti molestiae sapiente rem totam nemo quam iste. Sunt ut ea consequatur facilis, et, blanditiis, voluptas atque sint maiores hic laudantium.
                </p>
                <a href="" className="btn-text">Learn more &rarr;</a>
            </div>            
            <div className="col-1-of-2">
                <div className="composition">
                    <img src="img/small/15.jpg" alt="" className="composition__photo composition__photo--p1" />
                    <img src="img/small/10.jpg" alt="" className="composition__photo composition__photo--p2" />
                    <img src="img/small/5.jpg" alt="" className="composition__photo composition__photo--p3" />
                </div>
            </div>
        </div>
    </section>
  )
}
