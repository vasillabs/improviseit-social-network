import React from 'react'

export default function Header() {
  return (
    <header className="header">
        <div className="header__logo-box">
            <img src="img/logo.png" alt="Logo" className="header__logo" />
        </div>

        <div className="header__text-box">
                <h1 className="heading-primary">
                    <span className="heading-primary--main">Improvise</span>
                    <span className="heading-primary--sub">How about we spend some time together?</span>
                </h1>
                <a href="" className="btn btn--white btn--animated">Discover our tours</a>
        </div>
    </header>
  )
}
