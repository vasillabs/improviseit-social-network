import React from 'react'

export default function FeaturedEvents() {
  return (
    <section className="section-tours">
        <div className="u-center-text">
            <h2 className="heading-secondary u-margin-bottom-big">
                Most popular tours
            </h2>
        </div>

        <div className="row">
            <div className="col-1-of-3">
                <div className="card">
                    <div className="card__side card__side--front">
                        <div className="card__picture card__picture--1">
                            &nbsp;
                        </div>
                        <h4 className="card__heading">The sea explorer</h4>
                        <div className="card__details">Lorem ipsum dolor sit amet.</div>
                    </div>
                    <div className="card__side card__side--back card__side--back-1">BACK</div>
                </div>
            </div>
            <div className="col-1-of-3"></div>
            <div className="col-1-of-3"></div>
        </div>
    </section>
  )
}
