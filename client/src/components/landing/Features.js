import React from 'react'

export default function Features() {
  return (
    <section className="section-features">
        <div className="row">
            <div className="col-1-of-4">
                <div className="feature-box">
                    <i className="feature-box__icon icon-basic-keyboard"></i>
                    <h3 className="heading-tertiary u-margin-bottom-small">Heading</h3>
                    <p className="feature-box__text">Lorem ipsum dolor sit amet consasd asd asda ectetur adipisicing elit.</p>
                </div>
            </div>
            <div className="col-1-of-4">
                <div className="feature-box">
                    <i className="feature-box__icon icon-basic-trashcan-refresh"></i>
                    <h3 className="heading-tertiary u-margin-bottom-small">Heading</h3>
                    <p className="feature-box__text">Lorem ipsum dolor sit amet consecasd aasd asd asd asd aatetur adipisicing elit.</p>
                </div>
            </div>
            <div className="col-1-of-4">
                <div className="feature-box">
                    <i className="feature-box__icon icon-basic-usb"></i>
                    <h3 className="heading-tertiary u-margin-bottom-small">Heading</h3>
                    <p className="feature-box__text">Lorem ipsum dolor sit amet conse asda dasd ctetur adipisicing elit.</p>
                </div>
            </div>
            <div className="col-1-of-4">
                <div className="feature-box">
                    <i className="feature-box__icon icon-basic-share"></i>
                    <h3 className="heading-tertiary u-margin-bottom-small">Heading</h3>
                    <p className="feature-box__text">Lorem ipsum asdasd asd dolor sit amet consectetur adipisicing elit.</p>
                </div>
            </div>
        </div>
    </section>
  )
}
