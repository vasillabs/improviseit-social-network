import React, { Component } from 'react';
import Header from './Header';
import About from './About';
import Features from './Features';
import FeaturedEvents from './FeaturedEvents';
import Testimonials from './Testimonials';
import SignupForm from './SignupForm';
import LoginFrom from './LoginForm';
import '../../Landing.css';

export default class Landing extends Component {
  render() {
    return (
        <Header />
    <main>
        <About />
        <Features />
        <FeaturedEvents />
        <Testimonials />
        <SignupForm />
        <LoginFrom />
    </main>
    )
  }
}
